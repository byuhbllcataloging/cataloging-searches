# cataloging-searches

A repository of searches performed in the cataloging module of SirsiDynix's Workflows client.

# Search data

The file `searches.csv` contains a running list of searches performed. Each row consists of a single search performed using the Workflows client. The following is a list of fields that are used to describe a single search and help to recreate the search using Workflows. 

- `id` - A unique identifier for a single search specific to this list of searches
- `query` - The search string entered into the "Search for:" text field of the search wizard
- `module`  
- `group`  
- `wizard`
- `index`
- `library`
- `type`
- `class_scheme`
